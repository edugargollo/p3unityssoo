﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {
    public Bullet bulletPrefab;
 
    public Transform rotationArm;
    public Transform shootPoint;

    public Transform aimLeft, aimRight;
    protected Quaternion currentTargetRot, currentFromRot;
    public float rotationTime = 3;
    protected float currentRotationTime;


    public float afterShotTime = 1;
    public LayerMask shootLayer;

 
    protected float timeSinceLastShoot;
    protected enum states { rotating, afterShoot};
    protected states currentState = states.rotating;
    // Use this for initialization
	void Start ()
    {
        currentTargetRot = aimLeft.localRotation;
        currentFromRot = aimRight.localRotation;
    }
	
	// Update is called once per frame
	void Update ()
    {
        switch (currentState)
        {
            case states.rotating:
                rotate();
                checkIfICanSeeAnything();
                break;
            case states.afterShoot:
                timeSinceLastShoot += Time.deltaTime;
                if (timeSinceLastShoot >= afterShotTime)
                {
                    currentState = states.rotating;
                }
                break;
        }
	}

    protected void rotate()
    {
        currentRotationTime += Time.deltaTime;
        rotationArm.localRotation = Quaternion.Lerp(currentFromRot, currentTargetRot, currentRotationTime / rotationTime);

        if (currentRotationTime >= rotationTime)
        {
            var temp = currentFromRot;
            currentFromRot = currentTargetRot;
            currentTargetRot = temp;
            currentRotationTime = 0;
        }
    }

    protected void checkIfICanSeeAnything()
    {
        if(Physics.Raycast(shootPoint.position, shootPoint.forward,20, shootLayer))
        {
            currentState = states.afterShoot;
            timeSinceLastShoot = 0;

            Instantiate(bulletPrefab, shootPoint.position, shootPoint.rotation);

        }
    }
}
