﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    protected Transform target;
    public float speed = 2;
    protected Rigidbody rb;

	// Use this for initialization
	void Start () {
        target = FindObjectOfType<Shooter>().transform;
        rb = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        rb.MovePosition(transform.position + (target.position - transform.position).normalized * speed * Time.deltaTime);
	}
}
